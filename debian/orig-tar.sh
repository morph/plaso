#!/bin/sh

set -e

VERSION=$2
FILE=$3

NEWVERSION=${VERSION}+dfsg
NEWFILE=../plaso_${NEWVERSION}.orig.tar.xz

echo "Generating ${NEWFILE} ..."
zcat $FILE \
    | tar --wildcards --delete -f - 'plaso-*/test_data/' \
    | xz -c > ${NEWFILE}.t

mv ${NEWFILE}.t ${NEWFILE}
